//
//  FeatureCell.swift
//  Notflix
//
//  Created by salvatore frattolillo on 07/12/2019.
//  Copyright © 2019 salvatore frattolillo. All rights reserved.
//

import UIKit

class FeatureCell: UITableViewCell {
    
    var movieLink: String?
    
    @IBOutlet weak var Play: UIButton!
    @IBAction func PlayButton(_ sender: Any) {
        let name = Notification.Name("playButtonPressed")
        
        var notification = Notification(name: name)
        notification.object = URL(string: movieLink!)
        
        NotificationCenter.default.post(notification)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        Play.layer.cornerRadius = 2
    }

}
