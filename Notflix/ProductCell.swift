//
//  ProductCell.swift
//  Notflix
//
//  Created by salvatore frattolillo on 07/12/2019.
//  Copyright © 2019 salvatore frattolillo. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    var movieLink: String?
    
    @IBOutlet weak var movieImage: UIImageView!
    
    @IBAction func movieImagePressed(_ sender: UIButton) {
        let name = Notification.Name("playButtonPressed")
        
        var notification = Notification(name: name)
        notification.object = URL(string: movieLink!)
        
        NotificationCenter.default.post(notification)
        
    }
    
}
