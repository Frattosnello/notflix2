//
//  MoviesCollection.swift
//  Notflix
//
//  Created by salvatore frattolillo on 06/12/2019.
//  Copyright © 2019 salvatore frattolillo. All rights reserved.
//

import UIKit

class MoviesCollection: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    var movieApi = FakeAPI.getApi()
    
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieApi.movieFirstCollection.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Product Cell", for: indexPath)
            as! ProductCell
        
        cell.movieImage.image = movieApi.movieFirstCollection[indexPath.row].image
        cell.movieImage.layer.cornerRadius = cell.frame.size.height / 2
        cell.movieImage.layer.borderWidth = 1
        cell.movieImage.layer.masksToBounds = false
        cell.movieImage.layer.borderColor = UIColor.black.cgColor
        cell.movieImage.clipsToBounds = true
        cell.movieLink = movieApi.getMovies()[indexPath.row].url
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return CGSize(width: 100, height: 100)
        
    }
    
    


}
