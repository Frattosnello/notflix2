//
//  Movies2Collection.swift
//  Notflix
//
//  Created by salvatore frattolillo on 13/12/2019.
//  Copyright © 2019 salvatore frattolillo. All rights reserved.
//

import UIKit

class Movies2Collection: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let api = FakeAPI.getApi()
    
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return api.movieSecondCollection.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Product2 Cell", for: indexPath)
            as! Product2Cell
        
        cell.movieImage.image = api.movieSecondCollection[indexPath.row].image
        cell.movieLink = api.movieSecondCollection[indexPath.row].url
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return CGSize(width: 80, height: 120)
        
    }
    
    


}
