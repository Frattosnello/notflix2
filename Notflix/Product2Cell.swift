//
//  Product2Cell.swift
//  Notflix
//
//  Created by salvatore frattolillo on 13/12/2019.
//  Copyright © 2019 salvatore frattolillo. All rights reserved.
//

import UIKit

class Product2Cell: UICollectionViewCell {
    @IBOutlet weak var movieImage: UIImageView!
    
    var movieLink: String?
    
    
    @IBAction func playButtonPressed(_ sender: UIButton) {
        let name = Notification.Name("playButtonPressed")

        var notification = Notification(name: name)
        notification.object = URL(string: movieLink!)
        
        NotificationCenter.default.post(notification)
    }
    
}
