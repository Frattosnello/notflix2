//
//  HomeTable.swift
//  Notflix
//
//  Created by salvatore frattolillo on 06/12/2019.
//  Copyright © 2019 salvatore frattolillo. All rights reserved.
//

import UIKit
import AVKit
import NotificationCenter

class HomeTable: UITableView, UITableViewDelegate, UITableViewDataSource {
    
    var movieUrl: URL?
    let api = FakeAPI.getApi()
    var movieApi: FakeAPI = FakeAPI.getApi()
    var numberOfMoviesToShow: Int?

    
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieApi.getTotalRows()
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        
        if row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Feature Cell") as! FeatureCell
            tableView.rowHeight = 631
            cell.movieLink = movieApi.mainMovie.url
            tableView.separatorStyle = .none
            return cell
        }
        else if row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Category Cell") as! CategoryCell
            tableView.rowHeight = 150
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Category2 Cell") as! Category2Cell
            tableView.rowHeight = 150
            return cell
        }
        
    }
}

