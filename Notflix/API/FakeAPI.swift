//
//  FakeAPI.swift
//  Notflix
//
//  Created by salvatore frattolillo on 13/12/2019.
//  Copyright © 2019 salvatore frattolillo. All rights reserved.
//

import Foundation

class FakeAPI {
    private static let singleton = FakeAPI()
    
    var mainMovie: Movie = Movie(movieTitle: "The 100",
                                 image: #imageLiteral(resourceName: "image1.jpg"),
                                 attributes: "Action -- Fantasy -- Comedy -- Crime",
                                 plot: "...",
                                 url: "https://lnx.farmaciaeuropa.it/ada/fratto4.mp4")
    
    var movieFirstCollection: [Movie] = [Movie(movieTitle: "V Wars",
                                                image: #imageLiteral(resourceName: "image2.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/fratto8.mp4"),
                                          Movie(movieTitle: "Atypical",
                                                image: #imageLiteral(resourceName: "image3.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/fratto1.mp4"),
                                          Movie(movieTitle: "The end of the F***ing World 2",
                                                image: #imageLiteral(resourceName: "image4.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/fratto5.mp4"),
                                          Movie(movieTitle: "Jumanji",
                                                image: #imageLiteral(resourceName: "image5.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/fratto3.mp4"),
                                          Movie(movieTitle: "BirdBox",
                                                image: #imageLiteral(resourceName: "image6.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/fratto2.mp4"),
                                          
                                          Movie(movieTitle: "Vis a Vis 4",
                                                image: #imageLiteral(resourceName: "image7.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/fratto7.mp4"),
                                          
                                          Movie(movieTitle: "The Witcher",
                                                image: #imageLiteral(resourceName: "image8.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/fratto6.mp4"),
                                          
                                          Movie(movieTitle: "Il Cavaliere Oscuro il Ritorno",
                                                image: #imageLiteral(resourceName: "image9.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/video4.mov"),
                                          
                                          Movie(movieTitle: "Il Cavaliere Oscuro il Ritorno",
                                                image: #imageLiteral(resourceName: "image10.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/video12.mov")
        
    ]
    
    var movieSecondCollection: [Movie] = [Movie(movieTitle: "Il Cavaliere Oscuro il Ritorno",
                                                image: #imageLiteral(resourceName: "image11.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/video11.mov"),
                                          Movie(movieTitle: "Il Cavaliere Oscuro il Ritorno",
                                                image: #imageLiteral(resourceName: "image12.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/video2.mov"),
                                          Movie(movieTitle: "Il Cavaliere Oscuro il Ritorno",
                                                image: #imageLiteral(resourceName: "image13.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/video13.mov"),
                                          Movie(movieTitle: "Il Cavaliere Oscuro il Ritorno",
                                                image: #imageLiteral(resourceName: "image14.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/video14.mov"),
                                          Movie(movieTitle: "Il Cavaliere Oscuro il Ritorno",
                                                image: #imageLiteral(resourceName: "image15.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/video15.mov"),
                                          
                                          Movie(movieTitle: "Il Cavaliere Oscuro il Ritorno",
                                                image: #imageLiteral(resourceName: "image16.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/video16.mov"),
                                          
                                          Movie(movieTitle: "Il Cavaliere Oscuro il Ritorno",
                                                image: #imageLiteral(resourceName: "image17.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/video17.mov"),
                                          
                                          Movie(movieTitle: "Il Cavaliere Oscuro il Ritorno",
                                                image: #imageLiteral(resourceName: "image18.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/video18.mov"),
                                          
                                          Movie(movieTitle: "Il Cavaliere Oscuro il Ritorno",
                                                image: #imageLiteral(resourceName: "image19.jpg"),
                                                attributes: "Action -- Fantasy",
                                                plot: "...",
                                                url: "https://lnx.farmaciaeuropa.it/ada/video19.mov")
        
    ]
    
    private init() {
        
    }
    
    static func getApi() -> FakeAPI {
        return singleton
    }
    
    func getMovies() -> [Movie]{
        return movieFirstCollection
    }
    
    func getTotalRows() -> Int {
        return 30
    }
    
}
