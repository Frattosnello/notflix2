//
//  PlayerViewController.swift
//  Notflix
//
//  Created by salvatore frattolillo on 13/12/2019.
//  Copyright © 2019 salvatore frattolillo. All rights reserved.
//

import UIKit
import AVKit

class PlayerViewController: AVPlayerViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        player?.play()
            
    }
}
