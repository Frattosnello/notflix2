//
//  ViewController.swift
//  Notflix
//
//  Created by salvatore frattolillo on 05/12/2019.
//  Copyright © 2019 salvatore frattolillo. All rights reserved.
//

import UIKit
import AVKit
import NotificationCenter

class ViewController: UIViewController {
    var movieUrl: URL?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let name = Notification.Name("playButtonPressed")
        
        NotificationCenter.default.addObserver(self, selector: #selector(openPlayer), name: name, object: movieUrl)
        
        print("subscribed to notification center")
        

    }
    
    @objc func openPlayer(_ notification: Notification){
        
        print("notification received")
        
        let url = notification.object as! URL
        
        let playerViewController = storyboard?.instantiateViewController(identifier: "PlayerViewController") as! PlayerViewController
        
        playerViewController.player = AVPlayer(url: url)
        
        self.present(playerViewController, animated: true, completion: nil)
        
        
    }


}

